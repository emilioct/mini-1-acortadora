#!/usr/bin/python3

import socket

mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# Vuelve a usar el puerto cuando no hay ningún proceso usándolo
mySocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
# Enlaza el servidor a localhost y el puerto 1234
mySocket.bind(('localhost', 1234))

# Máximo 5 peticiones TCP
mySocket.listen(5)
BUFFER_SIZE = 1024
lista_url = {}


def listaToLinkSinAcortar(lista: dict):
    s: str = ""
    i: int = 1
    for key in lista:
        s = s + "<p> URL " + str(i) + " sin acortar: </p>" + "<a href=" + lista[key] + ">" + lista[key] + "</a>"
        i = i + 1
    return s


def listaToLinkAcortadas(lista: dict):
    t: str = ""
    i: int = 1
    for key in lista:
        t = t + "<p> URL " + str(i) + " acortada: </p>" + "<a href=" + lista[key] + ">" + key + "</a>"
        i = i + 1
    return t


try:
    while True:
        print('Waiting for connections')
        (recvSocket, address) = mySocket.accept()
        resource = recvSocket.recv(BUFFER_SIZE)
        resource_decoded = resource.decode('utf-8')
        petition = resource_decoded
        if 'url=' in petition:
            print("AQUI")
            petition = petition.split("url=")
            petition = petition[1].split("&")
            url: str = petition[0]
            print("Mi url es: ")
            print(url)
            if 'http%3A%2F%2F' in url:
                url = url.split("http%3A%2F%2F")[1]
                url = 'http://' + url
            elif 'https%3A%2F%2F' in url:
                url = url.split("https%3A%2F%2F")[1]
                url = 'https://' + url
            else:
                url = 'https://'+url
            short: str = petition[1]
            short = short.split("=")[1]
            lista_url[short] = url
            mi_lista = lista_url
            print(str(mi_lista))
            recvSocket.send(b"HTTP/1.1 200\r\n\r\n" +
                            b"<body>"
                            b" <form action= '/' method= 'POST'/>" +
                            b" URL to shorten: <input name=url type=text />" +
                            b" URL short: <input name=short type=text />" +
                            b"<input type='submit' value='Submit' />" +
                            b"<p>--------------Lista de URLs sin acortar--------------</p>" +
                            bytes(listaToLinkSinAcortar(mi_lista), 'utf-8') +
                            b"<p>--------------Lista de URLs acortadas--------------</p>" +
                            bytes(listaToLinkAcortadas(mi_lista), 'utf-8') +
                            b"</form>" +
                            b"\r\n")
            recvSocket.close()
        else:
            petition = resource_decoded
            petition = petition.split()
            if len(petition) > 1:
                petition = petition[1]
                petition = petition.split('/')
            else:
                petition = petition
                print(petition)

            if len(petition) > 0:
                myResource = petition[1]
            else:
                myResource = '/'
            if len(myResource) > 0:
                if myResource in lista_url:
                    new_url = lista_url[myResource]
                    print(new_url)
                    recvSocket.send(b"HTTP/1.1 301\r\n\r\n" +
                                    b"<html><body><h1>You will be redirected to: " +
                                    bytes(new_url, 'utf-8') +
                                    b" in 5 seconds</h1>" +
                                    b"<meta http-equiv='refresh' content='5;URL=" +
                                    bytes(new_url, 'utf-8') + b"'></body></html>"
                                                              b"\r\n")
                    recvSocket.close()
                else:
                    recvSocket.send(b"HTTP/1.1 400 \r\n\r\n" +
                                    b"<html><head><h1>ERROR 400 BAD PETITION"b"</head></html>" +
                                    b"<html><body><h1>Recurso no disponible " +
                                    b"</body></html>"
                                    b"\r\n")
                    recvSocket.close()
            else:
                petition = resource_decoded
                if 'url=' in petition:
                    petition = petition.split("url=")
                    petition = petition[1].split("&")
                    url: str = petition[0]
                    if 'http%3A%2F%2F' in url:
                        url = url.split("http%3A%2F%2F")[1]
                        url = 'http://' + url
                    elif 'https%3A%2F%2F' in url:
                        url = url.split("https%3A%2F%2F")[1]
                        url = 'https://' + url
                    else:
                        url = 'https://' + url
                    short: str = petition[1]
                    short = short.split("=")[1]
                    lista_url[short] = url
                recvSocket.send(b"HTTP/1.1 200\r\n\r\n" +
                                b"<body>" +
                                b" <form action= '/' method= 'POST'/>" +
                                b" URL to shorten: <input name=url type=text />" +
                                b" URL short: <input name=short type=text />" +
                                b"<input type='submit' value='Submit' />" +
                                b"<p> Lista de URLs sin acortar</p>" +
                                bytes(listaToLinkSinAcortar(lista_url), 'utf-8') +
                                b"<p> Lista de URLs acortadas</p>" +
                                bytes(listaToLinkAcortadas(lista_url), 'utf-8') +
                                b"</form>" +
                                b"\r\n")
                recvSocket.close()
except KeyboardInterrupt:
    print("Closing binded socket")
    mySocket.close()
